json.array!(@fundraisers) do |fundraiser|
  json.extract! fundraiser, :id, :name, :summary, :goal, :photo
  json.url fundraiser_url(fundraiser, format: :json)
end
