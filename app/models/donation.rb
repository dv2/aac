# == Schema Information
#
# Table name: donations
#
#  id                            :integer          not null, primary key
#  amount                        :integer          default(0), not null
#  currency                      :string(3)        default(""), not null
#  recurring                     :boolean          default(FALSE), not null
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  user_id                       :integer
#  fundraiser_id                 :integer
#  stripe_charge_id              :string(255)
#  first_name                    :string(255)
#  last_name                     :string(255)
#  email                         :string(255)
#  stripe_token                  :string(255)
#  stripe_customer_id            :string(255)
#  state                         :string(255)
#  error                         :string(255)
#  hide_amount                   :boolean          default(FALSE)
#  hide_name                     :boolean          default(FALSE)
#  comment                       :text(65535)
#  stripe_balance_transaction_id :string(255)
#  processing_currency           :string(255)
#  processing_amount             :integer
#  processing_fee                :integer
#  payout                        :integer
#
# Indexes
#
#  index_donations_on_email          (email)
#  index_donations_on_fundraiser_id  (fundraiser_id)
#

class Donation < ActiveRecord::Base
  belongs_to :user
  belongs_to :fundraiser

  accepts_nested_attributes_for :user

  validates :amount,
    presence: true,
    numericality: { greater_than: 0 }

  validates :currency,
    presence: true,
    inclusion: { in: ['usd', 'inr', 'eur', 'aud', 'gbp'] },
    length: { is: 3 }

  validates :recurring,
    #presence: true,
    inclusion: { in: [true, false] }
    # exclusion: { in: [nil] }

  def user_display_name
    "#{self.first_name} #{self.last_name}"
  end

  SUPPORTED_CURRENCIES = [
    'usd', # United States Dollar, USD
    'inr', # Indian Rupee, INR
    'eur', # Euro, EUR
    'aud', # Australian Dollar, AUD
    'gbp'  # British Pound, GBP
  ]

  include AASM

  aasm column: 'state' do
    state :pending, initial: true
    state :processing_customer
    state :processing_charge
    state :finished
    state :errored

    event :process_customer, after: :create_customer do
      transitions from: :pending, to: :processing_customer
    end

    event :process_charge, after: :create_charge do
      transitions from: :processing_customer, to: :processing_charge
    end

    event :finish do
      transitions from: :processing_charge, to: :finished
    end

    event :fail_customer do
      transitions from: :processing_customer, to: :errored
    end

    event :fail_charge do
      transitions from: :processing_charge, to: :errored
    end
  end

  def create_customer
    customer = Stripe::Customer.create(
      :email => self.email,
      :card  => self.stripe_token
    )
    self.update_attribute(:stripe_customer_id, customer.id)
  rescue Stripe::StripeError => e
    error = "method: #{__method__}, line: #{__LINE__}, error: #{e.inspect}"
    self.update_attribute(:error, error)
    self.fail_customer!
  end

  def create_charge
    charge_amount = self.amount * 100
    charge_amount = charge_amount.to_i
    charge = Stripe::Charge.create(
      :customer    => self.stripe_customer_id,
      :amount      => charge_amount,
      :description => self.description,
      :currency    => self.currency
    )

    balance_transaction = Stripe::BalanceTransaction.retrieve(charge.balance_transaction)
    self.update_attributes(
      stripe_charge_id: charge.id,
      stripe_balance_transaction_id: charge.balance_transaction,
      processing_currency: balance_transaction.currency,
      processing_amount: balance_transaction.amount,
      processing_fee: balance_transaction.fee,
      payout: balance_transaction.net
    )
    self.finish!
  rescue Stripe::StripeError => e
    error = "method: #{__method__}, line: #{__LINE__}, error: #{e.inspect}"
    self.update_attribute(:error, error)
    self.fail_charge!
  end

  def description
    desc = ""
    fundraiser = self.fundraiser
    desc += "AAC Fundraiser_id: #{fundraiser.id} #{self.first_name} #{self.last_name}"

    desc
  end
end
