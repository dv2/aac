# == Schema Information
#
# Table name: site_pages
#
#  id         :integer          not null, primary key
#  identifier :string(255)
#  body       :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SitePage < ActiveRecord::Base
  validates :identifier, presence: true
end
