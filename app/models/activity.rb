# == Schema Information
#
# Table name: activities
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  resource_name     :string(255)
#  resource_id       :integer
#  action            :string(255)
#  request_remote_ip :string(255)
#  http_user_agent   :string(255)
#  http_host         :string(255)
#  http_referer      :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_activities_on_user_id  (user_id)
#

class Activity < ActiveRecord::Base
  belongs_to :user

  before_save :handle_long_data

  def self.create_activity(request, params, current_user = nil, resource_id = nil)
    user_id = current_user ? current_user.id : nil

    if params[:controller] == "high_voltage/pages"
      action = params[:id]
    else
      action = params[:action]
    end

    Activity.create(
      user_id: user_id,
      resource_name: params[:controller],
      resource_id: resource_id,
      action: action,
      request_remote_ip: request.remote_ip,
      http_user_agent: request.env['HTTP_USER_AGENT'],
      http_host: request.env['HTTP_HOST'],
      http_referer: request.env['HTTP_REFERER']
    )

  end

  def handle_long_data
    self.http_referer = self.http_referer[0..254] if self.http_referer
    self.http_user_agent = self.http_user_agent[0..254] if self.http_user_agent
  end
end
