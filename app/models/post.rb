# == Schema Information
#
# Table name: posts
#
#  id            :integer          not null, primary key
#  body          :text(65535)
#  image_id      :string(255)
#  fundraiser_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Post < ActiveRecord::Base
  belongs_to :fundraiser

  attachment :image, type: :image

  validates :body, presence: true

end
