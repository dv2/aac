# == Schema Information
#
# Table name: fundraisers
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  summary    :text(65535)
#  goal       :integer          default(0), not null
#  currency   :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  image_id   :string(255)
#  active     :boolean          default(TRUE)
#
# Indexes
#
#  index_fundraisers_on_user_id  (user_id)
#

class Fundraiser < ActiveRecord::Base
  belongs_to :user
  has_many :donations
  has_many :posts, dependent: :destroy

  attachment :image, type: :image

  validates :name, presence: true
  validates :summary, presence: true
  validates :goal, presence: true, numericality: { greater_than: 0 }
  validates :currency, presence: true
  # validates :user_id, presence: true
  validates :image, presence: true

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }

  before_destroy { self.image.delete }

  def amount_collected
    # self.valid_donations.map(&:amount).sum.round
    self.valid_donations.map(&:processing_amount).sum / 100
  end

  def user_full_name
    self.user.full_name if user
  end

  def to_param
    "#{self.id}-#{self.name}".parameterize
  end

  def valid_donations
    self.donations.where(state: 'finished').order(created_at: :desc)
  end
end
