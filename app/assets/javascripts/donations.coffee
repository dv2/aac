# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $("#button_100").click ->
    $(".donation_buttons").removeClass("btn-primary")
    $("#donation_amount_other").val("")
    $(this).addClass("btn-primary")
    $("#donation_amount").val(100)

  $("#button_75").click ->
    $(".donation_buttons").removeClass("btn-primary")
    $("#donation_amount_other").val("")
    $(this).addClass("btn-primary")
    $("#donation_amount").val(75)

  $("#button_50").click ->
    $(".donation_buttons").removeClass("btn-primary")
    $("#donation_amount_other").val("")
    $(this).addClass("btn-primary")
    $("#donation_amount").val(50)

  $("#button_25").click ->
    $(".donation_buttons").removeClass("btn-primary")
    $("#donation_amount_other").val("")
    $(this).addClass("btn-primary")
    $("#donation_amount").val(25)

  $("#donation_amount_other").click ->
    $(".donation_buttons").removeClass("btn-primary")
    $("#donation_amount").val('')

  $("#donation_amount_other").keyup ->
    val = $(this).val()
    $("#donation_amount").val(val)
