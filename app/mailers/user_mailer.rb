class UserMailer < ApplicationMailer

  # UserMailer.donation_completed(donation.id).deliver_now
  def donation_completed(donation_id)
    @donation = Donation.find(donation_id)
    subject = "Thank you for supporting #{@donation.fundraiser.name} on AidACause.org"
    @to = @donation.email
    mail(to: to, bcc: bcc, subject: subject)
  end

  def notify_fundraiser_organizer(donation_id)
    @donation = Donation.find(donation_id)
    subject = "New donation from #{@donation.user_display_name} for #{@donation.fundraiser.name} on AidACause.org"
    @to = @donation.fundraiser.user.email
    mail(to: to, bcc: bcc, subject: subject)
  end

  def email_donor(donation_id)
    @donation = Donation.find(donation_id)
    subject = "Thanks for supporting Kotak Salesian School in 2017"
    @to = @donation.email
    mail(to: to, bcc: bcc, subject: subject)
  end

  def all_donors_update(donation_id)
    @donation = Donation.find(donation_id)
    subject = "Updates regarding the Fundraiser for Kotak Salesian School for 2017"
    @to = @donation.email
    mail(to: to, bcc: bcc, subject: subject)
  end

  # UserMailer.test_email.deliver_now
  def test_email
    subject = "#{Rails.env.upcase} - Test email from - #{Rails.application.secrets.host_name}"
    @to = Rails.application.secrets.email_default_to
    mail(to: to, bcc: bcc, subject: subject)
  end

  def to
    @to
  end

  def cc
  end

  def bcc
    Rails.application.secrets.email_default_to
  end
end
