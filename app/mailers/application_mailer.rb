class ApplicationMailer < ActionMailer::Base
  default from: Rails.application.secrets.email_default_from
  layout 'mailer'
end
