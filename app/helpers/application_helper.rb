module ApplicationHelper
  def display_donation_name(donation)
    donation.hide_name ? "Anonymous" : donation.user_display_name
  end

  def display_donation_amount(donation)
    str = currency_symbol(donation.currency.downcase)
    str += donation.hide_amount ? " Anonymous" : donation.amount.round.to_s
    str.html_safe
  end

  def currency_symbol(currency)
    case currency
    when 'usd' then '<i class="fa fa-usd" aria-hidden="true"></i>'
    when 'inr' then '<i class="fa fa-inr" aria-hidden="true"></i>'
    when 'eur' then '<i class="fa fa-eur" aria-hidden="true"></i>'
    when 'gbp' then '<i class="fa fa-gbp" aria-hidden="true"></i>'
    else currency.upcase
    end
  end

  def display_donation_comment(donation)
    donation.comment
  end

  def get_id(params_id)
    params_id.scan(/\A\d+/).first
  end

  def show_line_breaks(text)
    return nil if text.blank?
    text.gsub("\r\n", "<br />").gsub("\n\n", "<br /><br />").gsub("\n", "<br />").html_safe
  end

  def donation_amount_in_usd(donation)
    return if donation.processing_currency.blank? ||
              donation.currency == donation.processing_currency ||
              donation.hide_amount

    str = ''
    str += '<span class="donation_amount_usd">'
    str += ' (<i class="fa fa-usd" aria-hidden="true"></i>'
    str += (donation.processing_amount.to_f / 100).to_s
    str += ') '
    str += '</span>'
    str.html_safe
  end

  def meta_description(description)
    if description.present?
      "#{description} - Aid A Cause - AidACause.org"
    else
      "AidACause is a non-profit organization with a mission to help individuals establish a diversified giving portfolio"
    end
  end

  def show_meta_tags
    return false if controller_name == 'fundraisers' && action_name == 'show'
    return true
  end
end
