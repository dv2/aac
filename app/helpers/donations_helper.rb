module DonationsHelper
  def donation_amounts
    [
      ['100', '$ 100.00'],
      ['75',  '$ 75.00'],
      ['50', ' $ 50.00'],
      ['25', ' $ 25.00']
    ]
  end
end
