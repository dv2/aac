class FundraisersController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update, :destroy]
  # before_action :authenticate_user!, only: [:new, :edit, :create, :update, :destroy]
  before_action :set_fundraiser, only: [:show, :edit, :update, :destroy]

  # GET /fundraisers
  # GET /fundraisers.json
  def index
    @fundraisers = Fundraiser.active.order(created_at: :desc)
    @fundraisers_inactive = Fundraiser.inactive.order(created_at: :desc)
    Activity.create_activity(request, params, current_user, nil)
  end

  # GET /fundraisers/1
  # GET /fundraisers/1.json
  def show
    Activity.create_activity(request, params, current_user, @fundraiser.id)
  end

  # GET /fundraisers/new
  def new
    @fundraiser = Fundraiser.new
  end

  # GET /fundraisers/1/edit
  def edit
    authorize! :manage, @fundraiser
  end

  # POST /fundraisers
  # POST /fundraisers.json
  def create
    @fundraiser = Fundraiser.new(fundraiser_params)
    @fundraiser.currency = 'USD'
    @fundraiser.user_id = current_user.id if current_user

    respond_to do |format|
      if @fundraiser.save
        Activity.create_activity(request, params, current_user, @fundraiser.id)

        if current_user
          format.html { redirect_to @fundraiser, notice: 'Fundraiser was successfully created.' }
        else
          format.html { redirect_to signup_path(fundraiser_id: @fundraiser.id) }
          format.json { render :show, status: :created, location: @fundraiser }
        end
      else
        format.html { render :new }
        format.json { render json: @fundraiser.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fundraisers/1
  # PATCH/PUT /fundraisers/1.json
  def update
    authorize! :manage, @fundraiser
    respond_to do |format|
      if @fundraiser.update(fundraiser_params)
        format.html { redirect_to @fundraiser, notice: 'Fundraiser was successfully updated.' }
        format.json { render :show, status: :ok, location: @fundraiser }
      else
        format.html { render :edit }
        format.json { render json: @fundraiser.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fundraisers/1
  # DELETE /fundraisers/1.json
  def destroy
    authorize! :manage, @fundraiser
    @fundraiser.destroy
    respond_to do |format|
      format.html { redirect_to fundraisers_url, notice: 'Fundraiser was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fundraiser
      @fundraiser = Fundraiser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fundraiser_params
      params.require(:fundraiser).permit(:name, :summary, :goal, :image, :remove_image)
    end
end
