class DonationsController < ApplicationController
  def index

  end

  def new
    @donation = Donation.new
    @donation.user = User.new
  end

  def create
    @donation = Donation.new(donation_params)
    @donation.currency = "USD"
    if params[:donation][:amount].to_i.zero?
      params[:donation][:amount] = params[:donation][:amount_other]
    end

    if @donation.save
      flash[:success] = "Thank you for your donation."
      redirect_to donations_path
    else
      render 'new'
    end
  end

  private

  def donation_params
    params.require(:donation).permit(:amount, :recurring, user_attributes: [:first_name, :last_name, :email])
  end
end
