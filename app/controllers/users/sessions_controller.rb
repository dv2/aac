class Users::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    super do
      if params[:fundraiser_id]
        fundraiser = Fundraiser.find(params[:fundraiser_id])
        user = User.find_by(email: params[:user][:email])
        fundraiser.user_id = user.id
        fundraiser.save
        redirect_to fundraiser_path(fundraiser) and return
      end
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end
