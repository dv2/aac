class HomeController < ApplicationController
  def index
    Activity.create_activity(request, params, current_user, nil)
  end

  def about
    Activity.create_activity(request, params, current_user, nil)
  end

  def contact
    Activity.create_activity(request, params, current_user, nil)
  end
end
