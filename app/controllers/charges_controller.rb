class ChargesController < ApplicationController

  def new
    fundraiser = Fundraiser.find(params[:fundraiser_id])
    Activity.create_activity(request, params, current_user, fundraiser.id)
  end

  def create
    validate_params_for_pci

    @fundraiser = Fundraiser.find(params[:fundraiser_id])
    Activity.create_activity(request, params, current_user, @fundraiser.id)

    hide_name = (params[:hideName] == "on") ? true : false
    hide_amount = (params[:hideAmount] == "on") ? true : false
    currency = params[:currency]

    donation = Donation.create(
                 amount: params[:donationAmount],
                 currency: currency.downcase,
                 recurring: false,
                 fundraiser_id: params[:fundraiser_id],
                 first_name: params[:firstName],
                 last_name: params[:lastName],
                 email: params[:email],
                 stripe_token: params[:stripeToken],
                 hide_name: hide_name,
                 hide_amount: hide_amount,
                 comment: params[:comment]
               )

    donation.process_customer!
    donation.process_charge!

    if donation.finished?
      UserMailer.donation_completed(donation.id).deliver_now
      UserMailer.notify_fundraiser_organizer(donation.id).deliver_now
      redirect_to fundraiser_path(@fundraiser), notice: "Thank you for your donation of #{donation.currency.upcase} #{donation.amount}."
    else
      flash.now[:error] = "Something went wrong. Your payment was not processed."
      render 'new'
    end
  rescue Stripe::CardError => e
    puts "in method: #{__method__}, line: #{__LINE__}, exception: #{e.inspect}"
    flash[:error] = e.message
    redirect_to fundraiser_path(@fundraiser), notice: "Something went wrong. Your payment was not processed. Error: #{e.inspect}"
  rescue Exception => e
    puts "in method: #{__method__}, line: #{__LINE__}, exception: #{e.inspect}"
    flash[:error] = e.message
    redirect_to fundraiser_path(@fundraiser), notice: "Something went wrong. Your payment was not processed. Error: #{e.inspect}"
  end

  private

  def description
    desc = ""
    desc += "#{params[:first_name]} #{params[:last_name]}"

    desc
  end

  def validate_params_for_pci
    raise "credit card hitting server" if params.inspect =~ /4242/ && Rails.env.test?
  end
end
