class CreateSitePages < ActiveRecord::Migration
  def change
    create_table :site_pages do |t|
      t.string :identifier
      t.text :body

      t.timestamps null: false
    end
  end
end
