class ChangeAmountFieldsToIntegers < ActiveRecord::Migration
  def up
    change_column :donations, :amount, :integer
    change_column :fundraisers, :goal, :integer
  end
  def down
    change_column :donations, :amount, :decimal, default: 0.0, null: false, precision: 8, scale: 2
    change_column :fundraisers, :goal, :decimal, default: 0.0, null: false, precision: 8, scale: 2
  end
end
