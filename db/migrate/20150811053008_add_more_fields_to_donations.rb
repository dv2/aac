class AddMoreFieldsToDonations < ActiveRecord::Migration
  def change
    add_column :donations, :stripe_token, :string
    add_column :donations, :stripe_customer_id, :string
    add_column :donations, :state, :string
    add_column :donations, :error, :string
  end
end
