class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.decimal :amount,    default: 0.0, null: false, precision: 8, scale: 2
      t.string  :currency,  default: '',  null: false, limit: 3
      t.boolean :recurring, default: 0,   null: false

      t.timestamps null: false
    end
  end
end
