class AddHideFieldsToDonations < ActiveRecord::Migration
  def change
    add_column :donations, :hide_amount, :boolean, :default => false
    add_column :donations, :hide_name, :boolean, :default => false
    add_column :donations, :comment, :text
  end
end
