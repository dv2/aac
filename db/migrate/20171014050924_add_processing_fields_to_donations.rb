class AddProcessingFieldsToDonations < ActiveRecord::Migration
  def change
    add_column :donations, :stripe_balance_transaction_id, :string
    add_column :donations, :processing_currency, :string
    add_column :donations, :processing_amount, :integer
    add_column :donations, :processing_fee, :integer
    add_column :donations, :payout, :integer
  end
end
