class CreateFundraisers < ActiveRecord::Migration
  def change
    create_table :fundraisers do |t|
      t.string :name
      t.text :summary
      t.decimal :goal, default: 0.0, null: false, precision: 8, scale: 2
      t.string :currency
      t.string :photo

      t.timestamps null: false
    end
  end
end
