class AddFieldsToDonations < ActiveRecord::Migration
  def change
    add_column :donations, :stripe_charge_id, :string
    add_column :donations, :first_name, :string
    add_column :donations, :last_name, :string
    add_column :donations, :email, :string

    add_index :donations, :email
  end
end
