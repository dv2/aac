class AddActiveToFundraisers < ActiveRecord::Migration
  def change
    add_column :fundraisers, :active, :boolean, default: true
  end
end
