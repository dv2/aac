class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :body
      t.string :image_id
      t.integer :fundraiser_id

      t.timestamps null: false
    end
  end
end
