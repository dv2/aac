class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.integer :user_id
      t.string :resource_name
      t.integer :resource_id
      t.string :action
      t.string :request_remote_ip
      t.string :http_user_agent
      t.string :http_host
      t.string :http_referer

      t.timestamps null: false
    end

    add_index :activities, :user_id
  end
end
