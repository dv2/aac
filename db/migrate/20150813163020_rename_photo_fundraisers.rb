class RenamePhotoFundraisers < ActiveRecord::Migration
  def change
    remove_column :fundraisers, :photo, :string
    add_column :fundraisers, :image_id, :string
  end
end
