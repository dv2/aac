class AddFundraiserIdToDonations < ActiveRecord::Migration
  def change
    add_column :donations, :fundraiser_id, :integer
    add_index :donations, :fundraiser_id
  end
end
