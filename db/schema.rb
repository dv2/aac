# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171015061004) do

  create_table "activities", force: :cascade do |t|
    t.integer  "user_id",           limit: 4
    t.string   "resource_name",     limit: 255
    t.integer  "resource_id",       limit: 4
    t.string   "action",            limit: 255
    t.string   "request_remote_ip", limit: 255
    t.string   "http_user_agent",   limit: 255
    t.string   "http_host",         limit: 255
    t.string   "http_referer",      limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "activities", ["user_id"], name: "index_activities_on_user_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.boolean  "active",      limit: 1,   default: true
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "donations", force: :cascade do |t|
    t.integer  "amount",                        limit: 4,     default: 0,     null: false
    t.string   "currency",                      limit: 3,     default: "",    null: false
    t.boolean  "recurring",                     limit: 1,     default: false, null: false
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.integer  "user_id",                       limit: 4
    t.integer  "fundraiser_id",                 limit: 4
    t.string   "stripe_charge_id",              limit: 255
    t.string   "first_name",                    limit: 255
    t.string   "last_name",                     limit: 255
    t.string   "email",                         limit: 255
    t.string   "stripe_token",                  limit: 255
    t.string   "stripe_customer_id",            limit: 255
    t.string   "state",                         limit: 255
    t.string   "error",                         limit: 255
    t.boolean  "hide_amount",                   limit: 1,     default: false
    t.boolean  "hide_name",                     limit: 1,     default: false
    t.text     "comment",                       limit: 65535
    t.string   "stripe_balance_transaction_id", limit: 255
    t.string   "processing_currency",           limit: 255
    t.integer  "processing_amount",             limit: 4
    t.integer  "processing_fee",                limit: 4
    t.integer  "payout",                        limit: 4
  end

  add_index "donations", ["email"], name: "index_donations_on_email", using: :btree
  add_index "donations", ["fundraiser_id"], name: "index_donations_on_fundraiser_id", using: :btree

  create_table "fundraisers", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.text     "summary",    limit: 65535
    t.integer  "goal",       limit: 4,     default: 0,    null: false
    t.string   "currency",   limit: 255
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "user_id",    limit: 4
    t.string   "image_id",   limit: 255
    t.boolean  "active",     limit: 1,     default: true
  end

  add_index "fundraisers", ["user_id"], name: "index_fundraisers_on_user_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.text     "body",          limit: 65535
    t.string   "image_id",      limit: 255
    t.integer  "fundraiser_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "site_pages", force: :cascade do |t|
    t.string   "identifier", limit: 255
    t.text     "body",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "phone",                  limit: 255
    t.string   "address1",               limit: 255
    t.string   "address2",               limit: 255
    t.string   "city",                   limit: 255
    t.string   "state",                  limit: 255
    t.string   "zip",                    limit: 255
    t.string   "country",                limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.boolean  "admin",                  limit: 1,   default: false
    t.boolean  "super_admin",            limit: 1,   default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
