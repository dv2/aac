Raven.configure do |config|
  config.dsn = 'https://56a811fe147747c89cceef0958d61d4a:2435800691224a2598757636ce9fa640@sentry.io/231634'
  config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
end
