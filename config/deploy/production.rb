role :app, ["#{ENV['AAC_HOSTUSER']}@#{ENV['AAC_HOSTDOMAIN']}"]
role :web, ["#{ENV['AAC_HOSTUSER']}@#{ENV['AAC_HOSTDOMAIN']}"]
role :db,  ["#{ENV['AAC_HOSTUSER']}@#{ENV['AAC_HOSTDOMAIN']}"]

set :stage, :production

server ENV['AAC_HOSTDOMAIN'], user: ENV['AAC_HOSTUSER'], roles: %w{web app}, my_property: :my_value
