namespace :email do

  # bin/rake email:all_donors
  task :all_donors => :environment do
    puts ">>>> start task -- email:all_donors"

    fundraiser = Fundraiser.find(2)
    fundraiser.valid_donations.each do |donation|
      UserMailer.email_donor(donation.id).deliver_now
    end

    puts ">>>> end task -- email:all_donors"
  end

  # bin/rake email:all_donors_update
  task :all_donors_update => :environment do
    puts ">>>> start task -- email:all_donors_update"

    fundraiser = Fundraiser.find(2)
    fundraiser.valid_donations.each do |donation|
      UserMailer.all_donors_update(donation.id).deliver_now
    end

    puts ">>>> end task -- email:all_donors_update"
  end
end
