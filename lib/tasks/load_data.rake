namespace :data do

  task :load => :environment do
    puts " -- start data:load"
    User.delete_all
    user = User.create(
        first_name: "Dv",
        last_name: "Suresh",
        email: "dv@example.com",
        password: "password"
      )
    puts "user: #{user.inspect}"

    fundraiser = Fundraiser.create(
      name: "fund one",
      summary: "fund summ",
      goal: 2500,
      currency: "USD",
      user_id: user.id,
      image: File.open(File.join(Rails.root, "test/fixtures/files/test_image.jpg"))
      )

    fundraiser = Fundraiser.create(
      name: "#ProjectWestAfrica",
      summary: "Have you wanted to travel to Africa? What about do some volunteer work while there? What if you could go without spending anything?
      Wouldn´t that be nice. Well, we want to make that happen for someone.
      Myself, an adventure travel guide and part-time blogger (smallworldtravels.com ) and Johnny Ward, an online entrepreneur and travel blogger (onestep4ward.com ) are embarking on a trans African journey from Cape Town – Casablanca later this year. We are avid backpackers and we personally have our own funds to travel. What we want to do is something a bit unordinary though.",
      goal: 2500,
      currency: "USD",
      user_id: user.id,
      image: File.open(File.join(Rails.root, "test/fixtures/files/project_africa.jpg"))
      )

    fundraiser = Fundraiser.create(
      name: "Liberation Iraq Christian & Yazidi",
      summary: "This three minute video was produced by the President of CYCI, Steve Maman, in order to give  you a glimpse of what is going on in Mosul, Iraq. It will help you understand why he, along with his team, is so passionate about this cause and will go above ordinary measures to complete what needs to be done.
      Months ago, ISIS took over the town of Mosul, Iraq. Christians, Yazidis and other religious minorities were driven out of their homes by fear. ISIS systematically killed men, women and children without pity.
      Many of the women and young girls are being held hostage for enslavement and sexual purposes. In captivity, they also suffer from malnutrition and physical abuse causing their overall health to deteriorate at an alarming rate. They are held in the worst conditions, often forced to sleep in cages. The women and children are then sold like cattle in markets as sex slaves to ISIS fighters and militants.
      Some of these children are as young as 8 years old. Not only are they at the mercy of the fighters, but also of the men still residing in Mosul; men of various ages, in some cases over 60 years of age. The rapes, abuse, and brutality they suffer at the hands of their captors are beyond imagination.
      There are many reports of owners that have decapitated innocent children and women because they refused to perform certain sexual acts. Those who comply are repeatedly beaten and serve as sex slaves to their owners, as well as their friends and guests, hardly ever limited to one predator at a time.
      Many young adolescents are sold to brothels, where they are raped 30 to 40 times a day by ISIS fighters. The U.N reports that ISIS militants have forced overly abused sex slaves to undergo virginity restoration surgery, up to 20 times after being raped and sold. This type of female genital mutilation is gaining popularity as these young girls may command premium prices on the market.",
      goal: 2500,
      currency: "USD",
      user_id: user.id,
      image: File.open(File.join(Rails.root, "test/fixtures/files/liberation.jpg"))
      )

    puts "fundraiser: #{fundraiser.inspect}"
    puts " -- end data:load"
  end

  task :clear_all => :environment do
    rows = User.delete_all
    puts "Cleared all users (#{rows})"

    Fundraiser.all.each do |fundraiser|
      if fundraiser.image_id
        fundraiser.image.delete
      end
    end
    rows = Fundraiser.delete_all
    puts "Cleared all fundraisers (#{rows})"
    rows = Donation.delete_all
    puts "Cleared all donations (#{rows})"

    # clear files
    path = "tmp/uploads/cache/."
    FileUtils.rm_rf(path)
    path = "tmp/uploads/store/."
    FileUtils.rm_rf(path)
  end

  # bin/rake data:server
  task :server => :environment do
    puts " -- start data:server"
    raise "This should only run on development box" if Rails.env.production?

    commands = []

    db_name_prod  = Rails.application.secrets.db_name_prod
    mysql_user    = Rails.application.secrets.host_mysql_user
    mysql_pwd     = Rails.application.secrets.host_mysql_pwd

    commands << "mysqldump -u#{mysql_user} -p#{mysql_pwd} #{db_name_prod} > /tmp/z.sql"
    commands.each do |command|
      ssh_dump_download_load(command)
    end
    puts " -- end data:server"
  end

  def ssh_dump_download_load(mysql_dump_command)
    puts " in #{__method__} start"

    start_time  = Time.zone.now
    user        = Rails.application.secrets.host_user
    server      = Rails.application.secrets.host_domain
    db_name_dev = Rails.application.secrets.db_name_dev

    Net::SSH.start(server, user) do |ssh|
      result = ssh.exec! mysql_dump_command

      command = "gzip -f /tmp/z.sql"
      result = ssh.exec! command
    end

    print " ====>> Downloading..."
    source_file = "/tmp/z.sql.gz"
    source_file_path = "#{user}@#{server}:#{source_file}"
    dest_file_path = "~/Downloads"
    command = "scp -p #{source_file_path} #{dest_file_path} "
    `#{command}`
    puts "  download complete"

    print " ====>> Unzipping..."
    command = "gunzip -f ~/Downloads/z.sql.gz"
    `#{command}`
    puts "  unzip complete"

    print " ====>> Uploading..."
    command = "mysql -uroot #{db_name_dev} < ~/Downloads/z.sql"
    `#{command}`
    puts "  upload complete"

    end_time = Time.zone.now
    time = end_time - start_time
    time = time.round(2)
    # puts "It took #{time} seconds. i.e #{TextCommon.humanize_time_taken(time)}"

    puts " in #{__method__} end"
  end
end
