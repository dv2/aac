require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_select "title", "Home | Aid A Cause"
    assert_template 'home/index'

    # assert_select "a[href=?]", root_path,     text: "Home"
    # assert_select "a[href=?]", about_path,    text: "About"
    # assert_select "a[href=?]", contact_path,  text: "Contact Us"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About Us | Aid A Cause"
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Contact Us | Aid A Cause"
  end
end
