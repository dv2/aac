require 'test_helper'

class PostsControllerTest < ActionController::TestCase
  setup do
    @fundraiser = fundraisers(:one)
    @post = posts(:one)
  end

  # test "should get index" do
  #   get :index
  #   assert_response :success
  #   assert_not_nil assigns(:posts)
  # end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create post" do
    assert_difference('Post.count') do
      post :create, post: { body: @post.body, image_id: @post.image_id, fundraiser_id: @fundraiser.id }
    end

    # assert_redirected_to post_path(assigns(:post))
    assert_redirected_to fundraiser_path(@fundraiser)
  end

  # test "should show post" do
  #   get :show, id: @post
  #   assert_response :success
  # end

  test "should get edit" do
    skip
    get :edit, id: @post
    assert_response :success
  end

  test "should update post" do
    skip
    patch :update, id: @post, post: { body: @post.body, image_id: @post.image_id }
    assert_redirected_to post_path(assigns(:post))
  end

  test "should destroy post" do
    skip
    assert_difference('Post.count', -1) do
      delete :destroy, id: @post
    end

    assert_redirected_to posts_path
  end
end
