require 'test_helper'

class FundraisersControllerTest < ActionController::TestCase
  setup do
    @fundraiser = fundraisers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fundraisers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fundraiser" do
    skip
    assert_difference('Fundraiser.count') do
      # post :create, fundraiser: { goal: @fundraiser.goal, name: @fundraiser.name, photo: @fundraiser.photo, summary: @fundraiser.summary }
      post :create, fundraiser: {
        goal: 3500,
        name: "fundraiser name",
        image: get_test_image_path,
        summary: "fundraiser summary" }
    end

    # assert_redirected_to fundraiser_path(assigns(:fundraiser))
    fundraiser = Fundraiser.last
    assert_redirected_to signup_path(fundraiser_id: fundraiser.id)
  end

  test "should show fundraiser" do
    get :show, id: @fundraiser
    assert_response :success
  end

  test "should get edit" do
    skip
    get :edit, id: @fundraiser
    assert_response :success
  end

  test "should update fundraiser" do
    skip
    patch :update, id: @fundraiser, fundraiser: { goal: @fundraiser.goal, name: @fundraiser.name, summary: @fundraiser.summary }
    assert_redirected_to fundraiser_path(assigns(:fundraiser))
  end

  test "should destroy fundraiser" do
    skip
    assert_difference('Fundraiser.count', -1) do
      delete :destroy, id: @fundraiser
    end

    assert_redirected_to fundraisers_path
  end
end
