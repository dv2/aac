require 'test_helper'
require 'stripe_mock'

class ChargesControllerTest < ActionController::TestCase
  setup do
    StripeMock.start
    user = create_regular_user
    @fundraiser = create_fundraiser(user)
    os = OpenStruct.new
    os.currency = 'usd'
    Stripe::BalanceTransaction.stubs(:retrieve).returns(os)
  end

  test "should get new" do
    get :new, fundraiser_id: @fundraiser.id
    assert_response :success
  end

  test "should create" do

    email_count = ActionMailer::Base.deliveries.count

    assert_difference 'Donation.count', 1 do
      post :create,
        fundraiser_id: @fundraiser.id,
        currency: 'USD',
        donationAmount: 125,
        firstName: "Foo",
        lastName: "Bar",
        email: 'foo@bar.com',
        comment: "foo bar",
        stripeToken: "foo"
    end
    assert_response 302
    assert_redirected_to fundraiser_path(@fundraiser)
    assert_equal flash[:notice], "Thank you for your donation of USD 125."

    assert_equal email_count + 2, ActionMailer::Base.deliveries.count
  end
end
