require "test_helper"

class RoutingTest < ActionDispatch::IntegrationTest
  it "resolves the homepage" do
    assert_routing "/", controller: "home", action: "index"
  end

  it "resolves the about page" do
    assert_routing "/about", controller: "home", action: "about"
  end

  it "resolves the contact page" do
    assert_routing "/contact", controller: "home", action: "contact"
  end


  it "resolves the donations new page" do
    # assert_routing "/donations/new", controller: "donations", action: "new"
  end
end
