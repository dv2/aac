ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
require "minitest/rails/capybara"
require 'mocha/test_unit'
include ActionView::Helpers::DateHelper

reporters = []
# reporters << Minitest::Reporters::DefaultReporter.new
reporters << Minitest::Reporters::SpecReporter.new
# reporters << Minitest::Reporters::ProgressReporter.new
# reporters << Minitest::Reporters::RubyMateReporter.new
# reporters << Minitest::Reporters::RubyMineReporter.new
# reporters << Minitest::Reporters::JUnitReporter.new
Minitest::Reporters.use! reporters

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end

class ActionController::TestCase
  include Devise::TestHelpers
end

Minitest.after_run do
  puts " in Minitest.after_run"

  path = "tmp/uploads/cache/."
  puts " clearing files in #{path}"
  FileUtils.rm_rf(path)

  path = "tmp/uploads/store/."
  puts " clearing files in #{path}"
  FileUtils.rm_rf(path)
end

def create_regular_user
  num = rand_num
  user = User.new
  user.first_name = "email_#{num}@example.com"
  user.last_name = "email_#{num}@example.com"
  user.email = "email_#{num}@example.com"
  user.password = "password_#{num}"
  user.password_confirmation = "password_#{num}"
  user.save

  return user
end

def create_admin_user
  num = rand_num
  user = User.new
  user.first_name = "email_#{num}@example.com"
  user.last_name = "email_#{num}@example.com"
  user.email = "email_#{num}@example.com"
  user.password = "password_#{num}"
  user.password_confirmation = "password_#{num}"
  user.admin = true
  user.save

  return user
end

def create_super_admin_user
  num = rand_num
  user = User.new
  user.first_name = "email_#{num}@example.com"
  user.last_name = "email_#{num}@example.com"
  user.email = "email_#{num}@example.com"
  user.password = "password_#{num}"
  user.password_confirmation = "password_#{num}"
  user.super_admin = true
  user.save

  return user
end

def create_fundraiser(user=nil)
  fundraiser = Fundraiser.new
  fundraiser.name     = Faker::Lorem.sentence
  fundraiser.summary  = Faker::Lorem.paragraph
  fundraiser.goal     = Faker::Commerce.price
  fundraiser.currency = "USD"
  fundraiser.user_id  = user.id if user
  fundraiser.image    = get_test_image
  fundraiser.save

  return fundraiser
end

def get_test_image
  File.open(File.join(Rails.root, "test/fixtures/files/test_image.jpg"))
end

def get_test_image_path
  File.join(Rails.root, "test/fixtures/files/test_image.jpg")
end

def create_donation(fundraiser)
  donation = Donation.new
  donation.amount         = Faker::Commerce.price
  donation.currency       = "usd"
  donation.recurring      = false
  donation.user_id        = nil
  donation.fundraiser_id  = fundraiser.id
  donation.first_name     = Faker::Name.first_name
  donation.last_name      = Faker::Name.last_name
  donation.email          = Faker::Internet.email
  donation.hide_name      = false
  donation.hide_amount    = false
  donation.comment        = ""
  donation.save

  return donation
end

def sign_in_user(user)
  click_link "Login"
  fill_in "Email", with: user.email
  fill_in "Password", with: user.password
  click_button "Log in"
end

def sign_out_user
  click_link "Logout"
end

def rand_num
  rand(9999999)
end

def assert_logged_in_header_links(user)
  within(:css, '#header') do
    page.must_have_link "About",    href: about_path,   count: 1
    page.must_have_link "Fundraisers",    href: fundraisers_path,   count: 1

    within("a#logo") do
      page.must_have_selector(:xpath, "//img[@src='/assets/aac_logo.png' and @alt='AidACause Logo']")
    end
    page.must_have_selector(:xpath, "//a[@id='logo' and @href='/' and @class='navbar-brand']")

    page.must_have_content "Logged in as #{user.email}"
    page.must_have_link "Account",    href: edit_user_registration_path,   count: 1
    page.must_have_link "Account",    href: "/users/edit",   count: 1
    page.must_have_link "Logout",    href: destroy_user_session_path,   count: 1
  end
end

def assert_logged_out_header_links
  within(:css, '#header') do
    page.must_have_link "About",    href: about_path,   count: 1
    page.must_have_link "Fundraisers",    href: fundraisers_path,   count: 1

    within("a#logo") do
      page.must_have_selector(:xpath, "//img[@src='/assets/aac_logo.png' and @alt='AidACause Logo']")
    end
    page.must_have_selector(:xpath, "//a[@id='logo' and @href='/' and @class='navbar-brand']")

    page.must_have_link "Login",   href: login_path,   count: 1
    page.must_have_link "Sign up",  href: signup_path,   count: 1
  end
end
