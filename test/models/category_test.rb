# == Schema Information
#
# Table name: categories
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  active      :boolean          default(TRUE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  def setup
    @category = Category.new(
      name: "category name",
      description: "description")
  end

  test "should be valid" do
    assert @category.valid?
  end

  test "name should be present" do
    @category.name = ""
    assert_not @category.valid?

    @category.name = "  "
    assert_not @category.valid?

    @category.name = nil
    assert_not @category.valid?
  end

  test "name should be unique" do
    category1 = Category.create(
      name: "name1")
    category2 = Category.new(
      name: "name1")
    assert_not category2.valid?
  end

  test "description is optional" do
    @category.description = ""
    assert @category.valid?

    @category.description = "  "
    assert @category.valid?

    @category.description = nil
    assert @category.valid?
  end

  test "is active by default" do
    category = Category.create(
      name: "name")
    assert_equal true, category.active?
  end

  test "is inactive if set to false" do
    category = Category.create(
      name: "name",
      active: false)
    assert_equal false, category.active?
  end
end
