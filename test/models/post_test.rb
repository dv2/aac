# == Schema Information
#
# Table name: posts
#
#  id            :integer          not null, primary key
#  body          :text(65535)
#  image_id      :string(255)
#  fundraiser_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'test_helper'

class PostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "dependent destroy" do
    fr = create_fundraiser
    post = Post.create(body: "post body",
      fundraiser: fr)

    fr_initial_count = Fundraiser.count
    post_initial_count = Post.count

    fr.destroy

    assert_equal 1, fr_initial_count - Fundraiser.count
    assert_equal 1, post_initial_count - Post.count
  end
end
