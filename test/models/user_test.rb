# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string(255)
#  last_name              :string(255)
#  email                  :string(255)      default(""), not null
#  phone                  :string(255)
#  address1               :string(255)
#  address2               :string(255)
#  city                   :string(255)
#  state                  :string(255)
#  zip                    :string(255)
#  country                :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  admin                  :boolean          default(FALSE)
#  super_admin            :boolean          default(FALSE)
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(
              first_name: "Peter",
              last_name: "Griffin",
              email: "peter@example.com",
              phone: "2102102100",
              address1: "Address Line 1",
              address2: "Address Line 2",
              city: "City",
              state: "State",
              zip: "10110",
              country: "US",
              password: "password"
              )
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "first_name should be present" do
    ["", "  ", nil].each do |value|
      @user.first_name = value
      assert_not @user.valid?, "#{value.inspect} should not be valid"
    end

    ["a", "A", "John"].each do |value|
      @user.first_name = value
      assert @user.valid?, "#{value.inspect} should be valid"
    end
  end

  test "last_name should be present" do
    ["", "  ", nil].each do |value|
      @user.last_name = value
      assert_not @user.valid?, "#{value.inspect} should not be valid"
    end

    ["a", "A", "Dow"].each do |value|
      @user.last_name = value
      assert @user.valid?, "#{value.inspect} should be valid"
    end
  end

  test "email should be present" do
    @user.email = ""
    assert_not @user.valid?
  end
  test "email should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?, "email should be unique"
  end
  test "email should accept valid addresses" do
    valid_addresses = %w[
      user@example.com
      USER@foo.COM
      A_US-ER@foo.bar.org
      first.last@foo.jp
      alice+bob@baz.com
    ]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  test "email should reject invalid addresses" do
    invalid_addresses = %w[
      user@example,com
      user_at_foo.org
      user.name@example.
      foo@bar_baz.com
      foo@bar+baz.com
    ]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "phone should be optional" do
    @user.phone = ""
    assert @user.valid?
  end

  # test for address1
  # test for address2
  # test for city
  # test for state
  # test for zip
  # test for country
end
