# == Schema Information
#
# Table name: donations
#
#  id                            :integer          not null, primary key
#  amount                        :integer          default(0), not null
#  currency                      :string(3)        default(""), not null
#  recurring                     :boolean          default(FALSE), not null
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  user_id                       :integer
#  fundraiser_id                 :integer
#  stripe_charge_id              :string(255)
#  first_name                    :string(255)
#  last_name                     :string(255)
#  email                         :string(255)
#  stripe_token                  :string(255)
#  stripe_customer_id            :string(255)
#  state                         :string(255)
#  error                         :string(255)
#  hide_amount                   :boolean          default(FALSE)
#  hide_name                     :boolean          default(FALSE)
#  comment                       :text(65535)
#  stripe_balance_transaction_id :string(255)
#  processing_currency           :string(255)
#  processing_amount             :integer
#  processing_fee                :integer
#  payout                        :integer
#
# Indexes
#
#  index_donations_on_email          (email)
#  index_donations_on_fundraiser_id  (fundraiser_id)
#

require 'test_helper'

class DonationTest < ActiveSupport::TestCase
  def setup
    @donation = Donation.new(
                  amount: 125.50,
                  currency: "usd",
                  recurring: true)
  end

  test "should be valid" do
    assert @donation.valid?
  end

  # amount
  test "amount should be present" do
    @donation.amount = ""
    assert_not @donation.valid?

    @donation.amount = "   "
    assert_not @donation.valid?

    @donation.amount = nil
    assert_not @donation.valid?
  end

  test "amount cannot be 0" do
    @donation.amount = 0
    assert_not @donation.valid?
  end

  test "amount cannot be negative value" do
    @donation.amount = -25.35
    assert_not @donation.valid?
  end

  # currency
  test "currency should be present" do
    @donation.currency = ""
    assert_not @donation.valid?

    @donation.currency = " "
    assert_not @donation.valid?

    @donation.currency = nil
    assert_not @donation.valid?
  end

  test "currency validation should accept valid currencies" do
    valid_currencies = %w[ usd inr eur aud gbp ]
    valid_currencies.each do |valid_currency|
      @donation.currency = valid_currency
      assert @donation.valid?, "#{valid_currency.inspect} should be valid"
    end
  end

  test "currency validation should reject invalid currencies" do
    invalid_currencies = %w[INR EUR GBP USDD]
    invalid_currencies.each do |invalid_currency|
      @donation.currency = invalid_currency
      assert_not @donation.valid?, "#{invalid_currency.inspect} should be invalid"
    end
  end

  # recurring
  test "recurring should be present" do
    @donation.recurring = ""
    assert_not @donation.valid?

    @donation.recurring = nil
    assert_not @donation.valid?
  end

  test "recurring should be true or false" do
    @donation.recurring = true
    assert @donation.valid?

    @donation.recurring = false
    assert @donation.valid?
  end

end
