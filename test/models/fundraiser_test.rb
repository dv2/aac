# == Schema Information
#
# Table name: fundraisers
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  summary    :text(65535)
#  goal       :integer          default(0), not null
#  currency   :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  image_id   :string(255)
#  active     :boolean          default(TRUE)
#
# Indexes
#
#  index_fundraisers_on_user_id  (user_id)
#

require 'test_helper'

class FundraiserTest < ActiveSupport::TestCase
  def setup
    @fundraiser = Fundraiser.new(
                    name: 'Fundraiser 1',
                    summary: 'This is first fund raiser',
                    goal: 2500.00,
                    currency: 'USD',
                    image: get_test_image
                  )
  end

  test "should be valid" do
    assert @fundraiser.valid?
  end

  test "name should be present" do
    @fundraiser.name = ""
    assert_not @fundraiser.valid?
  end

  test "summary should be present" do
    @fundraiser.summary = ""
    assert_not @fundraiser.valid?
  end

  test "goal should be present" do
    @fundraiser.goal = ""
    assert_not @fundraiser.valid?

    @fundraiser.goal = nil
    assert_not @fundraiser.valid?

    @fundraiser.goal = 0
    assert_not @fundraiser.valid?

    @fundraiser.goal = -10
    assert_not @fundraiser.valid?
  end

  test "currency should be present" do
    @fundraiser.currency = nil
    assert_not @fundraiser.valid?
  end

  test "image should be present" do
    @fundraiser.image_id = nil
    assert @fundraiser.valid?
  end

  test "image can be jpeg" do
    @fundraiser.image = File.open(File.join(Rails.root, "test/fixtures/files/test_image.jpg"))
    assert @fundraiser.valid?
  end

  test "image can be jpg" do
    @fundraiser.image = File.open(File.join(Rails.root, "test/fixtures/files/test_image.jpeg"))
    assert @fundraiser.valid?
  end

  test "image can be png" do
    @fundraiser.image = File.open(File.join(Rails.root, "test/fixtures/files/test_image.png"))
    assert @fundraiser.valid?
  end

  test "image can be gif" do
    @fundraiser.image = File.open(File.join(Rails.root, "test/fixtures/files/test_image.gif"))
    assert @fundraiser.valid?
  end

  test "image cannot be pdf" do
    @fundraiser.image = File.open(File.join(Rails.root, "test/fixtures/files/test_doc.pdf"))
    assert_not @fundraiser.valid?
  end

end
