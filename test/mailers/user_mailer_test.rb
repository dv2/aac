require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  def setup
  end

  test "donation completed" do
    user = create_regular_user
    fundraiser = create_fundraiser(user)
    donation = create_donation(fundraiser)

    email = UserMailer.donation_completed(donation.id)
    assert_emails 1 do
      email.deliver_now
    end

    to = []
    to << donation.email
    assert_equal to, email.to
    assert_nil email.cc

    bcc = []
    bcc << Rails.application.secrets.email_default_to
    assert_equal bcc, email.bcc

    from = []
    from << Rails.application.secrets.email_default_from
    assert_equal from, email.from

    assert_equal "Thank you for supporting #{fundraiser.name} on AidACause.org", email.subject

    # assert_match /Dear #{donation.first_name} #{donation.last_name},/, email.body.to_s
    # assert_equal "Thank you for supporting #{fundraiser.name}", email.body.to_s
    # assert_match /Donation Summary:/, email.body.to_s
    # assert_match /#{donation.currency.upcase} #{donation.amount} Donation Amount/, email.body.to_s
    # assert_match /You make good thing possible./, email.body.to_s
    # assert_match /With gratitude,/, email.body.to_s
    # assert_match /Organizer/, email.body.to_s
    # assert_match /Thanks again for your support!/, email.body.to_s
  end

  test "notify fundraiser organizer" do
    user = create_regular_user
    fundraiser = create_fundraiser(user)
    donation = create_donation(fundraiser)

    email = UserMailer.notify_fundraiser_organizer(donation.id)
    assert_emails 1 do
      email.deliver_now
    end

    to = []
    to << fundraiser.user.email
    assert_equal to, email.to
    assert_nil email.cc

    bcc = []
    bcc << Rails.application.secrets.email_default_to
    assert_equal bcc, email.bcc

    from = []
    from << Rails.application.secrets.email_default_from
    assert_equal from, email.from

    assert_equal "New donation from #{donation.user_display_name} for #{fundraiser.name} on AidACause.org", email.subject
  end

  test "email all donors" do
    user = create_regular_user
    fundraiser = create_fundraiser(user)
    donation1 = create_donation(fundraiser)
    donation2 = create_donation(fundraiser)
    donation3 = create_donation(fundraiser)

    email = UserMailer.email_donor(donation1.id)
    assert_emails 1 do
      email.deliver_now
    end

    to = []
    to << donation1.email
    assert_equal to, email.to
    assert_nil email.cc

    bcc = []
    bcc << Rails.application.secrets.email_default_to
    assert_equal bcc, email.bcc

    from = []
    from << Rails.application.secrets.email_default_from
    assert_equal from, email.from

    assert_equal "Thanks for supporting Kotak Salesian School in 2017", email.subject
  end

  test "email all donors update" do
    user = create_regular_user
    fundraiser = create_fundraiser(user)
    donation1 = create_donation(fundraiser)
    donation2 = create_donation(fundraiser)
    donation3 = create_donation(fundraiser)

    email = UserMailer.all_donors_update(donation1.id)
    assert_emails 1 do
      email.deliver_now
    end

    to = []
    to << donation1.email
    assert_equal to, email.to
    assert_nil email.cc

    bcc = []
    bcc << Rails.application.secrets.email_default_to
    assert_equal bcc, email.bcc

    from = []
    from << Rails.application.secrets.email_default_from
    assert_equal from, email.from

    assert_equal "Updates regarding the Fundraiser for Kotak Salesian School for 2017", email.subject
  end
end
