require "test_helper"

feature "Login page" do
  scenario "visit login page" do
    visit login_path

    user = create_regular_user

    assert_difference 'User.count', 0 do
      fill_in :user_email, with: user.email
      fill_in :user_password, with: user.password
      click_button "Log in"
    end

    assert_equal current_path, root_path
    page.must_have_content "Signed in successfully."
    assert_logged_in_header_links(user)

    click_link "Logout"
    assert_equal current_path, root_path
    page.must_have_content "Signed out successfully."
    assert_logged_out_header_links

  end
end
