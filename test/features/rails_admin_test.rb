require 'test_helper'

feature "Access admin page" do
  scenario "LOGGED OUT user" do
    visit root_path
    visit "/admin"

    assert_equal new_user_session_path, current_path
    page.must_have_content "You need to sign in or sign up before continuing."
  end

  scenario "LOGGED IN regular user" do
    visit root_path
    user = create_regular_user
    sign_in_user(user)

    visit "/admin"
    assert_equal root_path, current_path
    page.must_have_content "You are not authorized to access this page."
    page.wont_have_content "Site Administration"
  end

  scenario "LOGGED IN admin" do
    visit root_path
    user = create_admin_user
    sign_in_user(user)

    visit "/admin"
    assert_equal "/admin", current_path
    page.wont_have_content "You are not authorized to access this page."
    page.must_have_content "Site Administration"

    page.must_have_link "Categories",    href: "/admin/category", count: 2
    page.wont_have_link "Donations",    href: "/admin/donation", count: 2
    page.must_have_link "Fundraisers",  href: "/admin/fundraiser", count: 2
    page.must_have_link "Site pages",   href: "/admin/site_page", count: 2
    page.wont_have_link "Users",        href: "/admin/user", count: 2
  end

  scenario "LOGGED IN super_admin" do
    visit root_path
    user = create_super_admin_user
    sign_in_user(user)

    visit "/admin"
    assert_equal "/admin", current_path
    page.wont_have_content "You are not authorized to access this page."
    page.must_have_content "Site Administration"

    page.must_have_link "Categories",    href: "/admin/category", count: 2
    page.must_have_link "Donations",    href: "/admin/donation", count: 2
    page.must_have_link "Fundraisers",  href: "/admin/fundraiser", count: 2
    page.must_have_link "Site pages",   href: "/admin/site_page", count: 2
    page.must_have_link "Users",        href: "/admin/user", count: 2
  end
end
