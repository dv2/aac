require "test_helper"

feature "Sign up page" do
  scenario "visit sign up page" do
    visit signup_path

    email = "user_#{rand_num}@example.com"
    assert_difference 'User.count', 1 do
      fill_in :user_first_name, with: "user first name"
      fill_in :user_last_name, with: "user last name"
      fill_in :user_email, with: email
      fill_in :user_password, with: "password"
      fill_in :user_password_confirmation, with: "password"
      click_button "Sign Up"
    end

    user = User.last
    assert_equal user.first_name, "user first name"
    assert_equal user.last_name, "user last name"
    assert_equal user.email, email
    assert_logged_in_header_links(user)

    assert_equal current_path, root_path
    page.must_have_content "Welcome! You have signed up successfully."
    assert_logged_in_header_links(user)

    click_link 'Account'

    email = "user_#{rand_num}@example.com"
    assert_difference 'User.count', 0 do
      fill_in :user_first_name, with: "user first name -- update"
      fill_in :user_last_name, with: "user last name -- update"
      fill_in :user_email, with: email
      fill_in :user_current_password, with: "password"
      click_button "Update"
    end

    user = User.last
    assert_equal user.first_name, "user first name -- update"
    assert_equal user.last_name, "user last name -- update"
    assert_equal user.email, email
    assert_logged_in_header_links(user)

    assert_equal current_path, root_path
    page.must_have_content "Your account has been updated successfully."
    assert_logged_in_header_links(user)

    click_link "Logout"
    assert_equal current_path, root_path
    page.must_have_content "Signed out successfully."
    assert_logged_out_header_links

  end
end
