require 'stripe_mock'
require "test_helper"

feature "Fundraisers process" do

  # When NOT LOGGED IN
  # can visit fundraisers home page
  # Start A Fundraiser
  # User is taken to Sign up page
  # User can sign up/login
  # If user chooses to sign up
  # Fundraiser is created, new user registered and fundraiser assigned to the new user
  scenario "Fundraisers -- WHEN NOT LOGGED IN -- AND USER does SIGNUP" do
    assert_difference 'Activity.count', 1 do
      visit fundraisers_path
    end
    activity = Activity.last
    assert_equal nil,           activity.user_id
    assert_equal "fundraisers", activity.resource_name
    assert_equal nil,           activity.resource_id
    assert_equal "index",       activity.action
    assert_not   nil,           activity.request_remote_ip
    assert_not   nil,           activity.http_user_agent
    assert_not   nil,           activity.http_host
    assert_not   nil,           activity.http_referer

    assert_equal page.current_path, "/fundraisers"

    page.must_have_link "Start A Fundraiser", href: new_fundraiser_path, count: 1
    click_link "Start A Fundraiser"

    assert_equal page.current_path, new_fundraiser_path
    assert_equal page.current_path, "/fundraisers/new"

    assert_difference ['Fundraiser.count', 'Activity.count'], 1 do
      fill_in :fundraiser_name, with: "Fundraiser name"
      fill_in :fundraiser_summary, with: "Fundraiser summary"
      fill_in :fundraiser_goal, with: "2500"
      page.attach_file('fundraiser_image', get_test_image_path)
      # fill_in :fundraiser_photo, with: "http://www.imgur.com/abcdef"
      click_button "Start Fundraiser"
    end

    fundraiser = Fundraiser.last
    assert_equal fundraiser.name, "Fundraiser name"
    assert_equal fundraiser.summary, "Fundraiser summary"
    assert_equal fundraiser.goal.to_i, 2500
    assert_equal fundraiser.currency, "USD"
    # assert_equal fundraiser.photo, "http://www.imgur.com/abcdef"
    assert_equal fundraiser.user_id, nil

    activity = Activity.last
    assert_equal nil,           activity.user_id
    assert_equal "fundraisers", activity.resource_name
    assert_equal fundraiser.id, activity.resource_id
    assert_equal "create",      activity.action
    assert_not   nil,           activity.request_remote_ip
    assert_not   nil,           activity.http_user_agent
    assert_not   nil,           activity.http_host
    assert_not   nil,           activity.http_referer

    assert_equal current_path, signup_path
    within(:css, "#sign_up_top_message") do
      page.must_have_content "You're Almost Done"
      page.must_have_content "Create an Account or Log in to be taken to your fundraiser page."
      page.must_have_link "Log in", href: login_path(fundraiser_id: fundraiser.id), count: 1
    end
    page.must_have_link "Log in", href: login_path(fundraiser_id: fundraiser.id), count: 2

    assert_difference 'User.count', 1 do
      fill_in :user_first_name, with: "user first name"
      fill_in :user_last_name, with: "user last name"
      fill_in :user_email, with: "user_#{rand_num}@example.com"
      fill_in :user_password, with: "password"
      fill_in :user_password_confirmation, with: "password"
      click_button "Sign Up"
    end

    user = User.last
    assert_logged_in_header_links(user)

    fundraiser = Fundraiser.last
    assert_equal fundraiser.name, "Fundraiser name"
    assert_equal fundraiser.summary, "Fundraiser summary"
    assert_equal fundraiser.goal.to_i, 2500
    assert_equal fundraiser.currency, "USD"
    # assert_equal fundraiser.photo, "http://www.imgur.com/abcdef"
    assert_equal fundraiser.user_id, user.id

    assert_equal current_path, fundraiser_path(fundraiser)
    assert_equal page.title, "#{fundraiser.name} | Aid A Cause"

    page.must_have_content "Fundraiser name"
    page.must_have_content "Fundraiser summary"
    # page.must_have_content "Goal: 2500"
    page.must_have_link "Edit Fundraiser", href: edit_fundraiser_path(fundraiser), count: 1

    click_link "Edit Fundraiser"
    assert_equal current_path, edit_fundraiser_path(fundraiser)

    assert_difference ['Fundraiser.count', 'User.count'], 0 do
      fill_in :fundraiser_name, with: "Fundraiser name -- update"
      fill_in :fundraiser_summary, with: "Fundraiser summary -- update"
      fill_in :fundraiser_goal, with: "3500"
      # fill_in :fundraiser_photo, with: "http://www.imgur.com/abcdef_update"
      click_button "Update Fundraiser"
    end

    fundraiser = Fundraiser.find(fundraiser.id)
    assert_equal fundraiser.name, "Fundraiser name -- update"
    assert_equal fundraiser.summary, "Fundraiser summary -- update"
    assert_equal fundraiser.goal.to_i, 3500
    assert_equal fundraiser.currency, "USD"
    # assert_equal fundraiser.photo, "http://www.imgur.com/abcdef_update"
    assert_equal fundraiser.user_id, user.id
  end

  # When NOT LOGGED IN
  # can visit fundraisers home page
  # Start A Fundraiser
  # User is taken to Sign up page
  # User can sign up/login
  # If user chooses to login
  # Fundraiser is created and fundraiser assigned to the logged in user
  scenario "Fundraisers -- WHEN NOT LOGGED IN -- AND USER does LOGIN" do
    visit fundraisers_path
    assert_equal page.current_path, "/fundraisers"

    page.must_have_link "Start A Fundraiser", href: new_fundraiser_path, count: 1
    click_link "Start A Fundraiser"

    assert_equal page.current_path, new_fundraiser_path
    assert_equal page.current_path, "/fundraisers/new"

    assert_difference 'Fundraiser.count', 1 do
      fill_in :fundraiser_name, with: "Fundraiser name 2"
      fill_in :fundraiser_summary, with: "Fundraiser summary 2"
      fill_in :fundraiser_goal, with: "4500"
      page.attach_file('fundraiser_image', get_test_image_path)
      # fill_in :fundraiser_photo, with: "http://www.imgur.com/abcdef_2"
      click_button "Start Fundraiser"
    end

    fundraiser = Fundraiser.last
    assert_equal fundraiser.name, "Fundraiser name 2"
    assert_equal fundraiser.summary, "Fundraiser summary 2"
    assert_equal fundraiser.goal.to_i, 4500
    assert_equal fundraiser.currency, "USD"
    # assert_equal fundraiser.photo, "http://www.imgur.com/abcdef_2"
    assert_equal fundraiser.user_id, nil

    assert_equal current_path, signup_path
    within(:css, "#sign_up_top_message") do
      page.must_have_content "You're Almost Done"
      page.must_have_content "Create an Account or Log in to be taken to your fundraiser page."
      page.must_have_link "Log in", href: login_path(fundraiser_id: fundraiser.id), count: 1
    end
    page.must_have_link "Log in", href: login_path(fundraiser_id: fundraiser.id), count: 2

    within(:css, "#sign_up_top_message") do
      click_link "Log in"
    end

    within(:css, "#login_top_message") do
      page.must_have_content "Log in to AidACause"
      page.must_have_content "If you don't have an account yet, you should create one."
      page.must_have_link "create one", href: signup_path(fundraiser_id: fundraiser.id), count: 1
    end
    page.must_have_link "Sign up", href: signup_path(fundraiser_id: fundraiser.id), count: 1

    user = create_regular_user

    assert_difference 'User.count', 0 do
      fill_in :user_email, with: user.email
      fill_in :user_password, with: user.password
      click_button "Log in"
    end

    user = User.last
    assert_logged_in_header_links(user)

    fundraiser = Fundraiser.last
    assert_equal fundraiser.name, "Fundraiser name 2"
    assert_equal fundraiser.summary, "Fundraiser summary 2"
    assert_equal fundraiser.goal.to_i, 4500
    assert_equal fundraiser.currency, "USD"
    # assert_equal fundraiser.photo, "http://www.imgur.com/abcdef_2"
    assert_equal fundraiser.user_id, user.id

    assert_equal current_path, fundraiser_path(fundraiser)

    page.must_have_content "Fundraiser name"
    page.must_have_content "Fundraiser summary"
    page.must_have_content "$4,500"
    page.must_have_link "Edit Fundraiser", href: edit_fundraiser_path(fundraiser), count: 1

    click_link "Edit Fundraiser"
    assert_equal current_path, edit_fundraiser_path(fundraiser)

    assert_difference ['Fundraiser.count', 'User.count'], 0 do
      fill_in :fundraiser_name, with: "Fundraiser name -- update"
      fill_in :fundraiser_summary, with: "Fundraiser summary -- update"
      fill_in :fundraiser_goal, with: "3500"
      page.attach_file('fundraiser_image', get_test_image_path)
      # fill_in :fundraiser_photo, with: "http://www.imgur.com/abcdef_update"
      click_button "Update Fundraiser"
    end

    fundraiser = Fundraiser.find(fundraiser.id)
    assert_equal fundraiser.name, "Fundraiser name -- update"
    assert_equal fundraiser.summary, "Fundraiser summary -- update"
    assert_equal fundraiser.goal.to_i, 3500
    assert_equal fundraiser.currency, "USD"
    # assert_equal fundraiser.photo, "http://www.imgur.com/abcdef_update"
    assert_equal fundraiser.user_id, user.id
  end

  # When LOGGED IN
  # can visit fundraisers home page
  # Start A Fundraiser
  # Fundraiser is created and fundraiser assigned to the logged in user
  scenario "Fundraisers -- WHEN LOGGED IN" do
    visit root_path
    user = create_regular_user
    sign_in_user user

    visit fundraisers_path
    assert_equal page.current_path, "/fundraisers"

    page.must_have_link "Start A Fundraiser", href: new_fundraiser_path, count: 1
    click_link "Start A Fundraiser"

    assert_equal page.current_path, new_fundraiser_path
    assert_equal page.current_path, "/fundraisers/new"

    assert_difference 'Fundraiser.count', 1 do
      fill_in :fundraiser_name, with: "Fundraiser name 3"
      fill_in :fundraiser_summary, with: "Fundraiser summary 3"
      fill_in :fundraiser_goal, with: "5500"
      page.attach_file('fundraiser_image', get_test_image_path)
      # fill_in :fundraiser_photo, with: "http://www.imgur.com/abcdef_3"
      click_button "Start Fundraiser"
    end

    fundraiser = Fundraiser.last
    assert_equal fundraiser.name, "Fundraiser name 3"
    assert_equal fundraiser.summary, "Fundraiser summary 3"
    assert_equal fundraiser.goal.to_i, 5500
    assert_equal fundraiser.currency, "USD"
    # assert_equal fundraiser.photo, "http://www.imgur.com/abcdef_3"
    assert_equal fundraiser.user_id, user.id

    assert_equal current_path, fundraiser_path(fundraiser)
    page.must_have_content "Fundraiser was successfully created."
    assert_logged_in_header_links(user)

    page.must_have_content "Fundraiser name 3"
    page.must_have_content "Fundraiser summary 3"
    page.must_have_content "$5,500"
    page.must_have_link "Edit Fundraiser", href: edit_fundraiser_path(fundraiser), count: 1

    click_link "Edit Fundraiser"
    assert_equal current_path, edit_fundraiser_path(fundraiser)

    assert_difference ['Fundraiser.count', 'User.count'], 0 do
      fill_in :fundraiser_name, with: "Fundraiser name -- update"
      fill_in :fundraiser_summary, with: "Fundraiser summary -- update"
      fill_in :fundraiser_goal, with: "3500"
      page.attach_file('fundraiser_image', get_test_image_path)
      # fill_in :fundraiser_photo, with: "http://www.imgur.com/abcdef_update"
      click_button "Update Fundraiser"
    end

    fundraiser = Fundraiser.find(fundraiser.id)
    assert_equal fundraiser.name, "Fundraiser name -- update"
    assert_equal fundraiser.summary, "Fundraiser summary -- update"
    assert_equal fundraiser.goal.to_i, 3500
    assert_equal fundraiser.currency, "USD"
    # assert_equal fundraiser.photo, "http://www.imgur.com/abcdef_update"
    assert_equal fundraiser.user_id, user.id
  end

  scenario "Fundraiser Donation -- WHEN NOT LOGGED IN" do
    os = OpenStruct.new
    os.currency = 'usd'
    Stripe::BalanceTransaction.stubs(:retrieve).returns(os)

    StripeMock.start

    user = create_regular_user
    fundraiser = create_fundraiser(user)

    assert_difference 'Activity.count', 1 do
      visit fundraiser_path(fundraiser)
    end
    activity = Activity.last
    assert_equal nil,           activity.user_id
    assert_equal "fundraisers", activity.resource_name
    assert_equal fundraiser.id, activity.resource_id
    assert_equal "show",        activity.action
    assert_not   nil,           activity.request_remote_ip
    assert_not   nil,           activity.http_user_agent
    assert_not   nil,           activity.http_host
    assert_not   nil,           activity.http_referer

    within(:css, '#right_side') do
      page.must_have_link "Make A Donation", new_charge_path
      assert_difference 'Activity.count', 1 do
        click_link "Make A Donation"
      end
    end
    assert_equal current_path, new_charge_path

    activity = Activity.last
    assert_equal nil,           activity.user_id
    assert_equal "charges",     activity.resource_name
    assert_equal fundraiser.id, activity.resource_id
    assert_equal "new",         activity.action
    assert_not   nil,           activity.request_remote_ip
    assert_not   nil,           activity.http_user_agent
    assert_not   nil,           activity.http_host
    assert_not   nil,           activity.http_referer

    # assert_difference ['Donation.count', 'Activity.count'], 1 do
    assert_difference ['Donation.count'], 1 do
      fill_in :donationAmount, with: 225.35
      fill_in :firstName, with: 'FirstName'
      fill_in :lastName, with: 'LastName'
      fill_in :email, with: 'first@last.com'
      fill_in :cardNumber, with: '4242424242424242'
      fill_in :cardExpiryMM, with: '12'
      fill_in :cardExpiryYYYY, with: '2022'
      fill_in :cardCVC, with: '123'
      click_button "Make Donation"
    end

    # activity = Activity.last
    # assert_equal nil,           activity.user_id
    # assert_equal "charges",     activity.resource_name
    # assert_equal fundraiser.id, activity.resource_id
    # assert_equal "create",      activity.action
    # assert_not   nil,           activity.request_remote_ip
    # assert_not   nil,           activity.http_user_agent
    # assert_not   nil,           activity.http_host
    # assert_not   nil,           activity.http_referer

    donation = Donation.last
    assert_equal current_path, fundraiser_path(fundraiser)
    page.must_have_content "Thank you for your donation of USD #{donation.amount}."
    page.must_have_content ""

    donation = Donation.last
    assert_equal donation.amount, 225
    assert_equal donation.currency, "usd"
    assert_equal donation.recurring, false
    assert_equal donation.user_id, nil
    assert_equal donation.fundraiser_id, fundraiser.id
    assert_match /\Atest_ch_\S+/, donation.stripe_charge_id
    assert_match /\Atest_cus_\S+/, donation.stripe_customer_id
    # assert_match /\Atok_\S+/, donation.stripe_token
    assert_equal donation.first_name, "FirstName"
    assert_equal donation.last_name, "LastName"
    assert_equal donation.email, "first@last.com"
    assert_equal donation.state, "finished"
    assert_equal donation.error, nil

  end

  scenario "Fundraiser Donation Page - with few donations" do
    user = create_regular_user
    fundraiser = create_fundraiser(user)

    donation_1 = create_donation(fundraiser)

    donation_2 = create_donation(fundraiser)
    donation_2.update_attribute(:hide_name, true)

    donation_3 = create_donation(fundraiser)
    donation_3.update_attribute(:hide_amount, true)

    donation_4 = create_donation(fundraiser)
    donation_4.update_attribute(:comment, 'hello testing comment')

    visit fundraiser_path(fundraiser)

    within(:css, '#fundraiser_show_name') do
      page.must_have_content fundraiser.name
    end

    within(:css, "#fundraiser_image") do
      # page.must_have_selector(:xpath, "//img[@src='/assets/aac_logo.png' and @alt='AidACause Logo']")
      # page.must_have_selector(:xpath, "//img[@src='/assets/aac_logo.png' and @alt='AidACause Logo']")
    end

    within(:css, "#right_side") do
      within(:css, "#fundraiser_stats") do
        # test for location
        # amount_collected = donation_1.amount + donation_2.amount + donation_3.amount + donation_4.amount
        page.must_have_content "$#{fundraiser.amount_collected.round} of $#{fundraiser.goal.round}"
        page.must_have_content "Raised by #{fundraiser.valid_donations.count} people"
        page.must_have_link "Make A Donation", href: new_charge_path(fundraiser_id: fundraiser.id)
      end

      within(:css, "#created_by") do
        page.must_have_content "Created by #{fundraiser.user_full_name}"
        page.must_have_content "on: #{fundraiser.created_at.strftime("%B %d, %Y") }"
      end
    end

    within(:css, "#fundraiser_content") do
      within(:css, "#fundraiser_summary") do
        page.must_have_content fundraiser.summary
      end
    end

    within(:css, "#fundraiser_donations_list") do
      page.must_have_content "#{fundraiser.valid_donations.count} Donations"
      if donation_1.state == "finished"
        within(:css, "#donation_detail_#{donation_1.id}") do
          page.must_have_content "$#{donation_1.amount.round}"
          page.must_have_content "#{donation_1.user_display_name}"
        end
      end
      if donation_1.state == "finished"
        within(:css, "#donation_detail_#{donation_2.id}") do
          page.must_have_content "$#{donation_2.amount.round}"
          page.must_have_content "Anonymous"
        end
      end
      if donation_1.state == "finished"
        within(:css, "#donation_detail_#{donation_3.id}") do
          page.must_have_content "$ Anonymous"
          page.must_have_content "#{donation_3.user_display_name}"
        end
      end
      if donation_1.state == "finished"
        within(:css, "#donation_detail_#{donation_4.id}") do
          page.must_have_content "$#{donation_4.amount.round}"
          page.must_have_content "hello testing comment"
        end
      end
    end
  end

  scenario "Fundraiser Authorization Tests" do

    Fundraiser.delete_all

    ######################################################
    # NOT LOGGED IN
    ######################################################
    visit fundraisers_path
    # page.must_have_content "Current Fundraisers"
    page.must_have_content "No active fundraisers"
    page.must_have_link "Start A Fundraiser", href: new_fundraiser_path

    user = create_regular_user
    fundraiser = create_fundraiser(user)
    visit fundraisers_path
    page.must_have_content "Current Fundraisers"
    page.must_have_link fundraiser.name, href: fundraiser_path(fundraiser), count: 1
    page.must_have_content "$#{fundraiser.goal.round}"
    page.must_have_link "Start A Fundraiser", href: new_fundraiser_path
    page.wont_have_link "Show"
    page.wont_have_link "Edit"
    page.wont_have_link "Destroy"

    visit fundraiser_path(fundraiser)
    # wont have link
    page.wont_have_link "Edit Fundraiser"
    page.wont_have_link "Post Update"
    # cant access edit page
    visit edit_fundraiser_path(fundraiser)
    assert_equal new_user_session_path, current_path
    # page.must_have_content "You are not authorized to access this page."


    ######################################################
    # LOGIN as user
    ######################################################
    sign_in_user(user)

    visit fundraisers_path
    page.must_have_content "Current Fundraisers"
    page.must_have_link fundraiser.name, href: fundraiser_path(fundraiser), count: 1
    page.must_have_link "Start A Fundraiser", href: new_fundraiser_path
    page.wont_have_link "Show"
    page.wont_have_link "Edit"
    page.wont_have_link "Destroy"

    visit fundraiser_path(fundraiser)
    page.must_have_link "Edit Fundraiser", href: edit_fundraiser_path(fundraiser), count: 1
    page.must_have_link "Post Update", href: new_post_path(fundraiser_id: fundraiser.id)

    sign_out_user

    ######################################################
    # LOGIN as different user
    ######################################################
    user2 = create_regular_user
    sign_in_user(user2)

    visit fundraisers_path
    page.must_have_content "Current Fundraisers"
    page.must_have_link fundraiser.name, href: fundraiser_path(fundraiser), count: 1
    page.must_have_link "Start A Fundraiser", href: new_fundraiser_path
    page.wont_have_link "Show"
    page.wont_have_link "Edit"
    page.wont_have_link "Destroy"

    visit fundraiser_path(fundraiser)
    page.wont_have_link "Edit Fundraiser"
    page.wont_have_link "Post Update"

    sign_out_user

    ######################################################
    # LOGIN as admin
    ######################################################
    admin = create_admin_user
    sign_in_user(admin)

    visit fundraisers_path
    page.must_have_content "Current Fundraisers"
    page.must_have_link fundraiser.name, href: fundraiser_path(fundraiser), count: 1
    page.must_have_link "Start A Fundraiser", href: new_fundraiser_path
    page.wont_have_link "Show"
    page.wont_have_link "Edit"
    page.wont_have_link "Destroy"

    visit fundraiser_path(fundraiser)
    page.must_have_link "Edit Fundraiser", href: edit_fundraiser_path(fundraiser), count: 1
    page.must_have_link "Post Update", href: new_post_path(fundraiser_id: fundraiser.id)

    sign_out_user

    ######################################################
    # LOGIN as super_admin
    ######################################################
    super_admin = create_super_admin_user
    sign_in_user(super_admin)

    visit fundraisers_path
    page.must_have_content "Current Fundraisers"
    page.must_have_link fundraiser.name, href: fundraiser_path(fundraiser), count: 1
    page.must_have_link "Start A Fundraiser", href: new_fundraiser_path
    page.wont_have_link "Show"
    page.wont_have_link "Edit"
    page.wont_have_link "Destroy"

    visit fundraiser_path(fundraiser)
    page.must_have_link "Edit Fundraiser", href: edit_fundraiser_path(fundraiser), count: 1
    page.must_have_link "Post Update", href: new_post_path(fundraiser_id: fundraiser.id)

    sign_out_user
  end

  scenario "Fundraisers -- Error messages" do
    visit fundraisers_path
    click_link "Start A Fundraiser"
    click_button "Start Fundraiser"

    page.must_have_content "Please review the problems below:"
  end

  scenario "Fundraiser -- Update posts" do
    user = create_regular_user
    fundraiser = create_fundraiser(user)

    visit root_path
    sign_in_user(user)

    visit fundraiser_path(fundraiser)
    page.must_have_link "Post Update", href: new_post_path(fundraiser_id: fundraiser.id)

    click_link "Post Update"

    assert_difference 'Post.count', 1 do
      fill_in :post_body, with: "This is the update one"
      page.attach_file('post_image', get_test_image_path)

      click_button "Post Update"
    end

    assert_equal fundraiser_path(fundraiser), current_path
    page.must_have_content "Post was successfully created."

    visit fundraiser_path(fundraiser)

    post = Post.last

    page.must_have_content "This is the update one"
    within(:css, "#fr_update_#{post.id}") do
      within(:css, '.panel-heading') do
        page.must_have_content "Update"
        within(:css, '.post_created_at') do
          # "#{time_ago_in_words(post.created_at)} AGO"
          "AGO"
        end
      end
    end

  end
end
