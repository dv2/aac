require "test_helper"

feature "Activity Test" do
  scenario "LOGGED OUT" do
    assert_difference "Activity.count", 1 do
      visit root_path
    end
    activity = Activity.last
    assert_equal nil,           activity.user_id
    assert_equal "home",        activity.resource_name
    assert_equal nil,           activity.resource_id
    assert_equal "index",       activity.action
    assert_not   nil,           activity.request_remote_ip
    assert_not   nil,           activity.http_user_agent
    assert_not   nil,           activity.http_host
    assert_not   nil,           activity.http_referer

    assert_difference "Activity.count", 1 do
      visit about_path
    end
    activity = Activity.last
    assert_equal nil,           activity.user_id
    assert_equal "home",        activity.resource_name
    assert_equal nil,           activity.resource_id
    assert_equal "about",       activity.action
    assert_not   nil,           activity.request_remote_ip
    assert_not   nil,           activity.http_user_agent
    assert_not   nil,           activity.http_host
    assert_not   nil,           activity.http_referer

    assert_difference "Activity.count", 1 do
      visit contact_path
    end
    activity = Activity.last
    assert_equal nil,           activity.user_id
    assert_equal "home",        activity.resource_name
    assert_equal nil,           activity.resource_id
    assert_equal "contact",       activity.action
    assert_not   nil,           activity.request_remote_ip
    assert_not   nil,           activity.http_user_agent
    assert_not   nil,           activity.http_host
    assert_not   nil,           activity.http_referer

    assert_difference "Activity.count", 1 do
      visit fundraisers_path
    end
    activity = Activity.last
    assert_equal nil,           activity.user_id
    assert_equal "fundraisers", activity.resource_name
    assert_equal nil,           activity.resource_id
    assert_equal "index",       activity.action
    assert_not   nil,           activity.request_remote_ip
    assert_not   nil,           activity.http_user_agent
    assert_not   nil,           activity.http_host
    assert_not   nil,           activity.http_referer
  end
end
