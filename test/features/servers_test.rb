require 'test_helper'

feature "all servers working properly" do
  scenario "check https://aidacause.org" do
    visit "https://aidacause.org"
    assert_match /^https:/, current_url
  end

  scenario "check https://www.aidacause.org" do
    visit "https://www.aidacause.org"
    assert_match /^https:/, current_url
  end

  scenario "check aidacause.org" do
    skip
    visit "http://aidacause.org"
    assert_match /^https:/, current_url
  end

  scenario "check www.aidacause.org" do
    skip
    visit "http://www.aidacause.org"
    assert_match /^https:/, current_url
  end
end
