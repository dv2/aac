require "test_helper"

feature "Donation process" do
  scenario "can make recurring donation" do
    visit root_path

    within("#home_carousal_section") do
      # page.must_have_link "DONATE NOW", href: new_donation_path, count: 3
      # click_link "DONATE NOW", match: :first
    end

    # assert_equal page.current_path, new_donation_path

    # assert_difference ['Donation.count', 'User.count'], 1 do
    #   within("#new_donation") do
    #     fill_in :donation_amount_other, with: 100
    #     check('donation_recurring')

    #     fill_in :donation_user_attributes_first_name, with: "Peter"
    #     fill_in :donation_user_attributes_last_name,  with: "Griffin"
    #     fill_in :donation_user_attributes_email,      with: "peter@example.com"

    #     click_button 'Donate'
    #   end
    # end

    # donation = Donation.last
    # assert_equal donation.amount,    100.00
    # assert_equal donation.currency,  "USD"
    # assert_equal donation.recurring, true

    # user = User.last
    # assert_equal user.first_name, "Peter"
    # assert_equal user.last_name,  "Griffin"
    # assert_equal user.email,      "peter@example.com"

    # assert_equal page.current_path, donations_path
    # page.must_have_content "Thank you for your donation."
  end

  scenario "can make one time donation" do
    visit root_path

    within("#home_carousal_section") do
      # page.must_have_link "DONATE NOW", href: new_donation_path, count: 3
      # click_link "DONATE NOW", match: :first
    end

    # assert_equal page.current_path, new_donation_path

    # assert_difference ['Donation.count', 'User.count'], 1 do
    #   within("#new_donation") do
    #     fill_in :donation_amount_other, with: 200
    #     uncheck('donation_recurring')

    #     fill_in :donation_user_attributes_first_name, with: "Peter2"
    #     fill_in :donation_user_attributes_last_name,  with: "Griffin2"
    #     fill_in :donation_user_attributes_email,      with: "peter2@example.com"

    #     click_button 'Donate'
    #   end
    # end

    # donation = Donation.last
    # assert_equal donation.amount,    200.00
    # assert_equal donation.currency,  "USD"
    # assert_equal donation.recurring, false

    # user = User.last
    # assert_equal user.first_name, "Peter2"
    # assert_equal user.last_name,  "Griffin2"
    # assert_equal user.email,      "peter2@example.com"

    # assert_equal page.current_path, donations_path
    # page.must_have_content "Thank you for your donation."
  end

  scenario "show error message when data not entered" do
    visit root_path

    within("#home_carousal_section") do
      # page.must_have_link "DONATE NOW", href: new_donation_path, count: 3
      # click_link "DONATE NOW", match: :first
    end

    # assert_equal page.current_path, new_donation_path

    # assert_difference ['Donation.count', 'User.count'], 0 do
    #   within("#new_donation") do
    #     fill_in :donation_amount_other, with: ""

    #     click_button 'Donate'
    #   end
    # end

    # assert_equal page.current_path, donations_path
    # page.wont_have_content "Thank you for your donation."
    # page.must_have_content "Please select or enter an amount."
    # page.must_have_content "Amount can't be blank"
    # page.must_have_content "Amount is not a number"

    # page.must_have_content "User first name can't be blank"
    # page.must_have_content "User last name can't be blank"
    # page.must_have_content "User email can't be blank"
  end
end
