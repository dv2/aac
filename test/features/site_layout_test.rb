require "test_helper"

feature "Site Layout Test" do
  scenario "LOGGED OUT -- has content" do
    visit root_path
    assert_equal page.title, "Home | Aid A Cause"
    assert_logged_out_header_links
    assert_logged_out_footer_links

    within("#home_carousal_section") do
      # page.must_have_link "DONATE NOW", href: new_donation_path
    end
  end
end

feature "About Us page" do
  scenario "LOGGED OUT -- has content" do
    visit about_path
    assert_equal page.title, "About Us | Aid A Cause"
    assert_logged_out_header_links
    assert_logged_out_footer_links
  end
end

feature "Contact page" do
  scenario "LOGGED OUT -- has content" do
    visit contact_path
    assert_equal page.title, "Contact Us | Aid A Cause"
    page.must_have_content "We are working on this page. Please send us an email: support@aidacause.org"
    assert_logged_out_header_links
    assert_logged_out_footer_links
  end
end

feature "Privacy Policy page" do
  scenario "LOGGED OUT -- has content" do
    visit page_path('privacy_policy')
    assert_equal page.title, "Privacy Policy | Aid A Cause"
    assert_logged_out_header_links
    assert_logged_out_footer_links

    assert_equal "/privacy_policy", current_path
    page.must_have_content "Privacy Policy"
  end
end

feature "Terms of use page" do
  scenario "LOGGED OUT -- has content" do
    visit page_path('terms_of_use')
    assert_equal page.title, "Terms of use | Aid A Cause"
    assert_logged_out_header_links
    assert_logged_out_footer_links

    assert_equal "/terms_of_use", current_path
    page.wont_have_content "Terms of use"
  end
end

feature "FAQ page" do
  scenario "LOGGED OUT -- has content" do
    visit page_path('faq')
    assert_equal page.title, "FAQ | Aid A Cause"
    assert_logged_out_header_links
    assert_logged_out_footer_links

    assert_equal "/faq", current_path
    page.must_have_content "FAQ"
  end
end

feature "Fundraisers page" do
  scenario "LOGGED OUT -- has content" do
    visit fundraisers_path
    assert_equal page.title, "Fundraisers | Aid A Cause"
    page.must_have_content "Current Fundraisers"
  end
end

feature "Fundraiser show page" do
  scenario "LOGGED OUT -- has content" do
    fundraiser = create_fundraiser
    visit fundraiser_path(fundraiser)
    assert_equal page.title, "#{fundraiser.name} | Aid A Cause"
    page.must_have_content fundraiser.name
  end
end

feature "Charge page" do
  scenario "LOGGED OUT -- has content" do
    fundraiser = create_fundraiser
    visit new_charge_path(fundraiser_id: fundraiser.id)
    assert_equal page.title, "Payment | Aid A Cause"
  end
end

feature "New Donation page" do
  scenario "LOGGED OUT -- has content" do
    # visit new_donation_path
    # assert_logged_out_header_links
    # assert_logged_out_footer_links

    # assert_donate_page_form
  end
end

def assert_logged_out_header_links
  within('#header') do
    # page.must_have_link "Home",     href: root_path,    count: 1
    page.must_have_link "About Us",    href: about_path,   count: 1
    page.must_have_link "Fundraisers",    href: fundraisers_path,   count: 1

    within("a#logo") do
      page.must_have_selector(:xpath, "//img[@src='/assets/aac_logo.png' and @alt='AidACause Logo']")
    end
    page.must_have_selector(:xpath, "//a[@id='logo' and @href='/' and @class='navbar-brand']")
  end
end

def assert_logged_out_footer_links
  within('footer') do
    page.must_have_content "AidACause.org © 2015"
    page.must_have_link "Privacy Policy",       href: page_path('privacy_policy'), count: 1
    page.wont_have_link "Terms of use",         href: page_path('terms_of_use'), count: 1
    page.must_have_link "FAQ",                  href: page_path('faq'), count: 1
    page.must_have_link "Contact Us",           href: contact_path, count: 1
    page.must_have_link "Back to top",          href: "#", count: 1
  end
end

def assert_donate_page_form
  page.must_have_content "Impact Lives. Donate Now."
  page.must_have_content "I will donate:"
  within("#new_donation") do
    # page.must_have_selector(:xpath, "//input[@type='radio']")
    page.must_have_selector(:xpath, "//input[@type='text' and @id='donation_amount_other' and @value='' and @placeholder='$ Other Amount']")
    page.must_have_content "$10.00 is the minimum online donation."
    page.must_have_content "All donations are tax deductible."

    page.must_have_selector(:xpath, "//input[@type='checkbox' and @id='donation_recurring']")
    page.must_have_content "Make this a monthly donation."

    page.must_have_selector(:xpath, "//input[@type='submit' and @value='Donate']")

    page.must_have_content "Contact Information"

    page.must_have_content "First Name"
    page.must_have_selector(:xpath, "//input[@type='text' and @id='donation_user_attributes_first_name' and @value='' and @placeholder='First Name']")

    page.must_have_content "Last Name"
    page.must_have_selector(:xpath, "//input[@type='text' and @id='donation_user_attributes_last_name' and @value='' and @placeholder='Last Name']")

    page.must_have_content "Email Address"
    page.must_have_selector(:xpath, "//input[@type='email' and @id='donation_user_attributes_email' and @value='' and @placeholder='Email Address']")
  end
end
